//
//  Repository.swift
//  desafio-ios
//
//  Created by Felipe Lefèvre Marino on 2/25/17.
//  Copyright © 2017 Felipe Marino. All rights reserved.
//

import UIKit
import ObjectMapper

struct Shot: Mappable {
    var teaserImage: UIImage?
    var mainImage: UIImage?
    var teaserImageURL: String
    var viewsCount: Int
    var commentsCount: Int
    var likesCount: Int
    var avatarURL: String
    var username: String
    var mainImageURL: String
    var title: String
    var description: String
    
    /// This function can be used to validate JSON prior to mapping. Return nil to cancel mapping at this point
    public init?(map: Map) {
        teaserImageURL = ""
        viewsCount = 0
        commentsCount = 0
        likesCount = 0
        avatarURL = ""
        username = ""
        mainImageURL = ""
        title = ""
        description = ""
    }
    
    init(teaserImageURL: String, viewsCount: Int, commentsCount: Int, likesCount: Int,
         avatarURL: String, username: String, mainImageURL: String, title: String, description: String) {
        self.teaserImageURL = teaserImageURL
        self.viewsCount = viewsCount
        self.commentsCount = commentsCount
        self.likesCount = likesCount
        self.avatarURL = avatarURL
        self.username = username
        self.mainImageURL = mainImageURL
        self.title = title
        self.description = description
    }
    
    // Mappable
    public mutating func mapping(map: Map) {
        teaserImageURL      <- map["images.teaser"]
        viewsCount          <- map["views_count"]
        commentsCount       <- map["comments_count"]
        likesCount          <- map["likes_count"]
        avatarURL           <- map["user.avatar_url"]
        username            <- map["user.username"]
        mainImageURL        <- map["images.normal"]
        title               <- map["title"]
        description         <- map["description"]
    }
}
