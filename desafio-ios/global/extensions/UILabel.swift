//
//  UILabel.swift
//  desafio-ios
//
//  Created by Felipe Lefèvre Marino on 8/2/17.
//  Copyright © 2017 Felipe Marino. All rights reserved.
//

import Foundation
import UIKit
import SwiftSoup

extension UILabel {
    
    func setTextFrom(html: String) {
        var desc = ""
        do{
            let html = html
            let doc: Document = try SwiftSoup.parse(html)
            desc = try doc.text()
        }catch Exception.Error(_, let message){
            print(message)
        }catch{
            print("error")
        }
        
        text = desc
    }
}
