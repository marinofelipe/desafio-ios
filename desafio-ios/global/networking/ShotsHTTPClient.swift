//
//  RepositoryHTTPClient.swift
//  desafio-ios
//
//  Created by Felipe Lefèvre Marino on 2/26/17.
//  Copyright © 2017 Felipe Marino. All rights reserved.
//

import Foundation
import ObjectMapper

typealias CompletionShotsSuccess = (_ repositories: [Shot]) -> Void
typealias CompletionShotsFailure = (_ statusCode: StatusCode, _ response: Any?, _ error: Error?) -> Void

class ShotsHTTPClient: HTTPClient {
    
    class func getShots(page: Int, success: @escaping CompletionShotsSuccess, failure: @escaping CompletionShotsFailure) {
        
        let url = "\(Constants.API.kBaseUrl)\(Constants.API.kGetShots)?page=\(page)&per_page=12&access_token=\(Constants.ClientInfo.kAccessToken)"
        
        super.request(method: .GET, url: url, parameters: nil, success: { (statusCode, response) in
            
            var shots = [Shot]()
            let jsonItems = response.object
            
            guard !response.null() else {
                success([])
                return
            }
            
            if let _shots = Mapper<Shot>().mapArray(JSONString: jsonItems.rawString()!) {
                shots = _shots
            }
            
            success(shots)
        }) { (statusCode, response, error) in
            failure(statusCode, response, error)
        }
    }
}
