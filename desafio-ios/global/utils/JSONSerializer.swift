//
//  JSONSerializer.swift
//  desafio-ios
//
//  Created by Felipe Lefèvre Marino on 2/26/17.
//  Copyright © 2017 Felipe Marino. All rights reserved.
//

import SwiftyJSON

typealias CompletionJSONSuccess = (_ statusCode: StatusCode, _ response: JSONSerializer) -> Void
typealias CompletionJSONFailure = (_ statusCode: StatusCode, _ response: Any?, _ error: Error?) -> Void

class JSONSerializer {
    
    
    //TODO add throw - maybe
    var object: JSON
    
    init(_ object: Any) {
        self.object = JSON(object)
    }
    
    public func null() -> Bool {
        if object.null != nil {
            return true
        }
        return false
    }
}
