//
//  Constants.swift
//  desafio-ios
//
//  Created by Felipe Marino on 27/07/17.
//  Copyright © 2017 Felipe Marino. All rights reserved.
//

import Foundation

public struct Constants {
    
    struct API {
        static let kBaseUrl = "https://api.dribbble.com/v1/"
        static let kGetShots = "shots"
    }
    
    struct ClientInfo {
        static let kAccessToken = "715379abd5cbc7f4dd0845ab5856ba72af8f82c8fd73bf289cfd4c4e9f9cd4df"
    }
    
    struct Cell {
        static let kShotId = "ShotCell"
        static let kShotDetailId = "ShotDetailCell"
    }
    
    struct Shots {
        static let kFetchText = "Fetching Shots..."
        static let kImagePlaceholder = "shots-placeholder"
        static let kShareTitle = "Select a platform to share the shot."
    }
    
    struct Segue {
        static let kShowDetail = "ShowDetail"
    }
    
    struct Storyboard {
        static let kShotsVCId = "ShotsViewController"
    }
}
