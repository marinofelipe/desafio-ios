//
//  BaseViewController.swift
//  desafio-ios
//
//  Created by Felipe Lefèvre Marino on 8/3/17.
//  Copyright © 2017 Felipe Marino. All rights reserved.
//

import UIKit

class BaseViewController: UIViewController {
    
    @IBOutlet weak var tableView: UITableView!
    override var prefersStatusBarHidden: Bool {
        return false
    }

    override func viewDidLoad() {
        super.viewDidLoad()

        automaticallyAdjustsScrollViewInsets = true
    }
}
