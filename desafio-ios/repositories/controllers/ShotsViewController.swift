//
//  ViewController.swift
//  desafio-ios
//
//  Created by Felipe Marino on 2/24/17.
//  Copyright © 2017 Felipe Marino. All rights reserved.
//

import UIKit

class ShotsViewController: BaseViewController {

    @IBOutlet weak var activityIndicator: UIActivityIndicatorView!
    @IBOutlet weak var noDataLabel: UILabel!
    public var refreshControl: RefreshControl?
    public var shots:[Shot] = []
    fileprivate var page = 0
    fileprivate var isLoading = true
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        activityIndicator.startAnimating()
        noDataLabel.isHidden = true
        navigationItem.backBarButtonItem?.plain()
        configTableView()
        fetchShots()
    }
    
    // MARK: TableView
    private func configTableView() {
        refreshControl = RefreshControl.init(withTitle: Constants.Shots.kFetchText)
        refreshControl?.delegate = self
        if #available(iOS 10.0, *) {
            tableView.refreshControl = refreshControl
        } else {
            if let refreshControl = refreshControl {
                tableView.addSubview(refreshControl)
            }
        }
    }

    
    // MARK: - Fetch Repositories
    open func fetchShots() {
        self.isLoading = true
        activityIndicator.isHidden = false
        activityIndicator.startAnimating()
        ShotsHTTPClient.getShots(page: self.page + 1, success: { shots in
            guard shots.count > 0 else {
                self.isLoading = false
                self.activityIndicator.stopAnimating()
                return
            }
            
            DispatchQueue.main.async {
                self.shots += shots
                self.tableView.reloadData()
                self.page += 1
                self.isLoading = false
                self.noDataLabel.isHidden = true
                self.fetchEnded()
            }
        }) { (_, _, _) in
            //could handle errors by type
            self.noDataLabel.isHidden = false
            self.fetchEnded()
        }
    }
    
    private func fetchEnded() {
        isLoading = false
        refreshControl?.endRefreshing()
        activityIndicator.stopAnimating()
        activityIndicator.isHidden = true
    }
    
    // MARK: Navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == Constants.Segue.kShowDetail {
            if let destination = segue.destination as? ShotDetailViewController {
                if let selectedIndexPath = tableView.indexPathForSelectedRow {
                    destination.shot = shots[selectedIndexPath.row]
                }
            }
        }
    }
}

// MARK: - TableView Delegate
extension ShotsViewController: UITableViewDelegate {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1;
    }
    
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return 5.0
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 210.0
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 5.0
    }
}

// MARK: - TableView DataSource
extension ShotsViewController: UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return shots.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = UITableViewCell()
        if let cell = tableView.dequeueReusableCell(withIdentifier: Constants.Cell.kShotId, for: indexPath) as? ShotTableViewCell {
            
            cell.selectionStyle = .none
            
            let shot = shots[indexPath.row]
            
            cell.teaserImage.load(stringUrl: shot.teaserImageURL, completionImage: {
                self.shots[indexPath.row].teaserImage = cell.teaserImage.image
            })
            cell.viewsCount.text = String(shot.viewsCount)
            cell.commentsCount.text = String(shot.commentsCount)
            cell.likesCount.text = String(shot.likesCount)
            cell.title.text = shot.title
            
            return cell
        }
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        //TODO: Update to set number to a smooth scrolling
        if (indexPath.row > shots.count - 10) && !self.isLoading {
            fetchShots()
        }
    }
}

// MARK: RefreshControlDelegate
extension ShotsViewController: RefreshControlDelegate {
    func willRefresh() {
        fetchShots()
    }
}
