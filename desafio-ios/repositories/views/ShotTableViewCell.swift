//
//  RepositoryTableViewCell.swift
//  desafio-ios
//
//  Created by Felipe Marino on 2/24/17.
//  Copyright © 2017 Felipe Marino. All rights reserved.
//

import UIKit

class ShotTableViewCell: UITableViewCell {
    
    @IBOutlet weak var teaserImage: UIImageView!
    @IBOutlet weak var viewsCount: UILabel!
    @IBOutlet weak var commentsCount: UILabel!
    @IBOutlet weak var likesCount: UILabel!
    @IBOutlet weak var title: UILabel!
    
    //TODo:fixed images for views, comments and likes
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
