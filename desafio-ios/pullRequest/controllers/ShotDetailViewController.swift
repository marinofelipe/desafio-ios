//
//  RepositoryDetailViewController.swift
//  desafio-ios
//
//  Created by Felipe Marino on 2/24/17.
//  Copyright © 2017 Felipe Marino. All rights reserved.
//

import UIKit

class ShotDetailViewController: BaseViewController {

    public var shot: Shot?
    static var isPresented = false
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        title = shot?.title
        
        tableView.estimatedRowHeight = 400
        tableView.rowHeight = UITableViewAutomaticDimension
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        ShotDetailViewController.isPresented = true
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        ShotDetailViewController.isPresented = false
    }
    
    // MARK: Actions
    @IBAction func didTapShare(_ sender: Any) {
        share(withInitialText: "", image: shot?.mainImage)
    }
}

// MARK: - TableView Delegate
extension ShotDetailViewController: UITableViewDelegate {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1;
    }
    
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return 5.0
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 5.0
    }
}

// MARK: - TableView DataSource
extension ShotDetailViewController: UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = UITableViewCell()
        if let cell = tableView
            .dequeueReusableCell(withIdentifier: Constants.Cell.kShotDetailId, for: indexPath) as? ShotDetailTableViewCell {
            
            cell.selectionStyle = .none
            
            if let shot = shot {
                cell.avatar.load(stringUrl: shot.avatarURL, completionImage: nil)
                cell.username.text = shot.username
                cell.mainImage.load(stringUrl: shot.mainImageURL, completionImage: {
                    self.shot?.mainImage = cell.mainImage.image
                })
                cell.mainDescription.setTextFrom(html: shot.description)
            }
            
            return cell
        }
        
        return cell
    }
}
