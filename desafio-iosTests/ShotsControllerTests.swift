//
//  ShotsControllerTests.swift
//  desafio-ios
//
//  Created by Felipe Lefèvre Marino on 8/2/17.
//  Copyright © 2017 Felipe Marino. All rights reserved.
//

import XCTest
@testable import desafio_ios

class ShotsControllerTests: XCTestCase {
    var underTestController: ShotsViewController!
    
    override func setUp() {
        super.setUp()
        
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        underTestController = storyboard.instantiateViewController(withIdentifier: Constants.Storyboard.kShotsVCId) as! ShotsViewController
        
        UIApplication.shared.keyWindow?.rootViewController = underTestController
        
        XCTAssertNotNil(underTestController.preloadView())
    }
    
    override func tearDown() {
        underTestController = nil
        super.tearDown()
    }
    
    
    //test if components are as expected on load
    func testIfViewsAreCorrectlyConfigured() {
        XCTAssertEqual(underTestController.noDataLabel.isHidden, true)
        XCTAssertEqual(underTestController.activityIndicator.isAnimating, true)
        if #available(iOS 10.0, *) {
            XCTAssertNotNil(underTestController.tableView.refreshControl)
        }
    }
    
    //test how data states after fetch
    func testFetchingBeerReturn() {
        XCTAssertEqual(underTestController.shots.count, 0)
        let promise = expectation(description: "received array of shots")
        ShotsHTTPClient.getShots(page: 1, success: { beers in
            self.underTestController.shots = beers
            XCTAssertEqual(self.underTestController.shots.count, 12)
            promise.fulfill()
        }) { (_, _, _) in
            XCTFail("Did not receive array of shots")
        }
        
        self.waitForExpectations(timeout: 30.0, handler: nil)
    }
    
    // Performance
    func test_StartDownload_Performance() {
        measure {
            self.underTestController.fetchShots()
        }
    }
}
