//
//  APITests.swift
//  desafio-ios
//
//  Created by Felipe Lefèvre Marino on 8/2/17.
//  Copyright © 2017 Felipe Marino. All rights reserved.
//

import XCTest
import Alamofire
import SwiftyJSON
@testable import desafio_ios

enum TestingErrors: Error {
    case unknown
}

class APITests: XCTestCase {
    var underTestAlamofireManager: SessionManager!
    
    override func setUp() {
        super.setUp()
        underTestAlamofireManager = Alamofire.SessionManager.default
    }
    
    override func tearDown() {
        underTestAlamofireManager = nil
        super.tearDown()
    }
    
    // MARK: Alamofire testing
    
    //expecting return to be 200
    func testAlamofireManagerWithValidCall() {
        let url = "\(Constants.API.kBaseUrl)\(Constants.API.kGetShots)?access_token=\(Constants.ClientInfo.kAccessToken)"
        let promiseStatusCode = 200
        let promise = expectation(description: "received return status 200")
        underTestAlamofireManager.request(url, method: .get, parameters: nil, encoding: JSONEncoding.default, headers: nil).responseJSON { response in
            
            switch response.result {
            case .success:
                XCTAssertEqual(promiseStatusCode, response.response?.statusCode)
                promise.fulfill()
                break
            case .failure:
                XCTFail("Did not received return 200")
                break
            }
        }
        
        self.waitForExpectations(timeout: 30.0, handler: nil)
    }
    
    
    //expecting bad access return
    func testWithBadCredentials() {
        let url = "\(Constants.API.kBaseUrl)\(Constants.API.kGetShots)?access_token=715379abd5cbc7f4dd0845ab585"
        let promise = expectation(description: "received bad credentials message")
        underTestAlamofireManager.request(url, method: .get, parameters: nil, encoding: JSONEncoding.default, headers: nil).responseJSON { response in
            
            guard let responseJSON = response.result.value as? NSDictionary, let messageString = responseJSON.object(forKey: "message") as? String else {
                preconditionFailure("Could not parse as JSON")
            }
            
            switch response.result {
            case .success:
                XCTAssertEqual(messageString, "Bad credentials.")
                promise.fulfill()
                break
            case .failure:
                XCTFail("Did not received bad credentials message")
                break
            }
        }
        
        self.waitForExpectations(timeout: 30.0, handler: nil)
    }
    
    // MARK: HTTPClient tests
    
    //expecting result to be 200
    func testHTTPClientValidCall() {
        let promiseStatusCode: StatusCode = .success
        let promiseJSON = JSON.init([])
        let promise = expectation(description: "received status code 200 and response as JSON object")
        let url = "\(Constants.API.kBaseUrl)\(Constants.API.kGetShots)?access_token=\(Constants.ClientInfo.kAccessToken)"
        HTTPClient.request(method: .GET, url: url, parameters: nil, success: { (statusCode, jsonSerializer) in
            
            XCTAssertEqual(promiseStatusCode, statusCode)
            XCTAssertEqual(promiseJSON.type, jsonSerializer.object.type)
            promise.fulfill()
        }) { (satusCode, response, error) in
            XCTFail("Did not receive status code 200 and response as JSON object")
        }
        
        self.waitForExpectations(timeout: 30.0, handler: nil)
    }
    
    // MARK: ShotsHTTPClient Tests
    
    //expecting success block
    func testBeersHTTPClientValidCall() {
        let promise = expectation(description: "Did receive answer on success block")
        ShotsHTTPClient.getShots(page: 1, success: { (shots) in
            promise.fulfill()
        }) { (_, _, _) in
            XCTFail("Did not receive receive answer on success block")
        }
        
        self.waitForExpectations(timeout: 30.0, handler: nil)
    }
}
