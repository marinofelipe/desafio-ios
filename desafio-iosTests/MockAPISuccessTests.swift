//
//  MockAPISuccessTests.swift
//  desafio-ios
//
//  Created by Felipe Lefèvre Marino on 8/1/17.
//  Copyright © 2017 Felipe Marino. All rights reserved.
//

import XCTest
import OHHTTPStubs
import Alamofire
@testable import desafio_ios

class MockAPISuccessTests: XCTestCase {
    var underTestController: ShotsViewController!
    
    override func setUp() {
        super.setUp()
        
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        underTestController = storyboard.instantiateViewController(withIdentifier: Constants.Storyboard.kShotsVCId) as! ShotsViewController
        UIApplication.shared.keyWindow?.rootViewController = underTestController
        XCTAssertNotNil(underTestController.preloadView())

        OHHTTPStubs.setEnabled(true, for: Alamofire.SessionManager.default.session.configuration)
        stub(condition: isHost("api.dribbble.com")) { _ in
            // Stub it with "shots.json" stub file (which is in same bundle as self)
            guard let stubPath = OHPathForFile("shots.json", type(of: self)) else {
                preconditionFailure("Could not find expected file in test bundle")
            }
            return fixture(filePath: stubPath, status: 200, headers: nil)
        }
    }
    
    override func tearDown() {
        underTestController = nil
        super.tearDown()
    }
    
    // MARK: BeersHTTPClient tests
    
    //expecting success block
    func testBeersHTTPClientValidCall() {
        let promise = expectation(description: "Did receive answer on success block")
        ShotsHTTPClient.getShots(page: 1, success: { (shots) in
            promise.fulfill()
        }) { (_, _, _) in
            XCTFail("Did not receive receive answer on success block")
        }
        
        self.waitForExpectations(timeout: 30.0, handler: nil)
    }
    
    //test how data states after fetch
    func testFetchingBeerReturn() {
        XCTAssertEqual(underTestController.shots.count, 0)
        let promise = expectation(description: "received array of shots")
        ShotsHTTPClient.getShots(page: 1, success: { shots in
            self.underTestController.shots = shots
            
            //It expects 10 itens because it uses the stub file shots.json
            XCTAssertEqual(self.underTestController.shots.count, 12)
            promise.fulfill()
        }) { (_, _, _) in
            XCTFail("Did not receive array of shots")
        }
        
        self.waitForExpectations(timeout: 30.0, handler: nil)
    }

}
